<?php
// Send a test JSON request to api gateway for prepay
// 发送一个 合法的 json request 来测试服务器响应

$jsonStr = '{"goods_info":"Test_Product","mid":"100105100000011","out_trade_no":"201802048567163482469","pay_channel":"W","return_url":"https://demo.motionpay.org/motionpayAPI_JAVA_PUB/CallbackServlet","sign":"6F2EA42E7DFDB6526CF2223AEED47015E5BF67F5","spbill_create_ip":"192.168.1.1","terminal_no":"WebServer","total_fee":1}';


// Setup cURL
$ch = curl_init('https://api.motionpay.org/onlinePayment/v1_1/pay/prePay');
curl_setopt_array($ch, array(
    CURLOPT_POST => TRUE,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json'
    ),
    CURLOPT_SSL_VERIFYPEER => FALSE, 
    CURLOPT_POSTFIELDS => $jsonStr
));

// Send the request
$response = curl_exec($ch);

// Check for errors
if($response === FALSE){
    die(curl_error($ch));
}

// Print the date from the response
echo $response;
