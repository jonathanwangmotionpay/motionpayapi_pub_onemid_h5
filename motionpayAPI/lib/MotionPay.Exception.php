<?php

/**
 * MotionPay API Exception, We didn't use it for now
 * MotionPay支付API异常类 目前还没有使用
 * @author Jonathan
 *
 */
class MotionPayException extends Exception
{
    public function errorMessage()
    {
        return $this->getMessage();
    }
}
