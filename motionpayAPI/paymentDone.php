<?php
ini_set('date.timezone', 'Asia/Shanghai');
require_once "./lib/MotionPay.Config.php";
require_once "./lib/MotionPay.Api.php";
require_once "./lib/Log.php";

$logHandler = new CLogFileHandler(MotionPayConfig::getMotionPayLogFilename());
$log = Log::Init($logHandler, 15);

header("Content-Type:text/html;charset=utf-8");
$orderId = "";
$paymentAmount = "";
$mid = "";

if(isset($_GET['orderId'])) {
    $orderId = $_GET['orderId'];
}
if(isset($_GET['paymentAmount'])) {
    $paymentAmount = $_GET['paymentAmount'];
}
if(isset($_GET['mid'])) {
    $mid = $_GET['mid'];
}

if(strlen($orderId) == 0) {
    $orderId = $_SESSION["orderId"];
}
if(strlen($paymentAmount) == 0) {
    $paymentAmount = $_SESSION["paymentAmount"];
}
if(strlen($mid) == 0) {
    $mid = $_SESSION["mid"];
}
if(strlen($paymentAmount) == 0) {
    $total_fee = $_GET['total_fee'];
    if(strlen($total_fee) > 0) {
        $paymentAmount = $total_fee * 100;
    }
}
if(strlen($mid) == 0) {
    $trade_status = $_GET['trade_status'];
    $mid = MotionPayConfig::getMid( MotionPayConfig::ONLINE_MERCHANT);
    if ($trade_status == 'TRADE_FINISHED') {
        $mid = MotionPayConfig::getMid(MotionPayConfig::H5_MERCHANT);
    }
}
$log->INFO("the paymentAmount in paymentDone.php is:" . $paymentAmount);
$log->INFO("the mid in paymentDone.php is:" . $paymentAmount);
$orderinfo = array($mid, $orderId, $paymentAmount);
$log->writeOrderToCSVFile(MotionPayConfig::getMotionPayOrderCSVFilename(), $orderinfo);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
  <title>Motion Pay Sample Payment Page</title>

	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>  
	<meta charset="UTF-8"/>  
	<link href="web.css" rel="stylesheet" />
	<link rel="icon" href="images/MotionpayIcon.gif" type="image/gif" sizes="16x16"/>
	
<script>
var myTimeoutVar;

function checkPaymentResult() {
	var paid = false;
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "paymentNotify.php?mid=<?php echo $mid ?>&orderId=<?php echo $orderId ?>&paymentAmount=<?php echo $paymentAmount ?>&", true);
	xhr.onload = function (e) {
	  if (xhr.readyState === 4) {
	    if (xhr.status === 200) {
	      var resultStr = xhr.responseText;
	      console.log(resultStr);
	      resultStr = resultStr.trim();
	      // alert("resultStr is:#" + resultStr +"#");
	      if(resultStr == "paid") {
	    	  // alert("payment is done.");
	    	  paid = true;
	    	  var divToUpdate = document.getElementById("infor_box");
	    	  divToUpdate.innerHTML = "<br/><font class='cOrange' style='font-size: 25px;'>Thank you very much for your payment.</font>";
	    	  clearTimeout(myTimeoutVar);
	      }
	    } else {
	      console.error(xhr.statusText);
	    }
	  }
	};
	xhr.onerror = function (e) {
	  console.error(xhr.statusText);
	};
	if(paid == false) {
		xhr.send(null);
		myTimeoutVar = setTimeout(checkPaymentResult, 1000);
	}
}


function setTimeoutCheckFunc() {
	myTimeoutVar = setTimeout(checkPaymentResult, 1000);
}
setTimeoutCheckFunc();
</script>		
</head>
<body>
<!-- header -->
<div id="header">
  <div class="logo">
    <a href="#" class="logoImg logoPic" style="cursor: default;"></a>
  </div>
  <a href="#" class="aProblem">Tech Support</a>
</div>
<!--header-->

<!--content-->
<div class="width1003" >
  <div class="pay_infor"  >
  
    <!--  ##<?php echo $mid; ?>##  -->
    <p><font class="cOrange" style="font-size: 25px;">Order id:<?php echo $orderId ?></font></p>
    <br/>
    <div id="infor_box" class="infor_box" style="height:320px;">
    	<br/><font class='cOrange' style='font-size: 25px;'>Please wait for the payment verification.</font>
    </div>
  </div>
</div>
</body>
</html>