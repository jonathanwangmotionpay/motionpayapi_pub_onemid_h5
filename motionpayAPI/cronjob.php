<?php
// This php file is to check if the order is paid. You won't need it unless you are having trouble to get the call back notification.
// To enable this function. Run crontab command to put it into the crontab list.
// crontab crontab.file
// crontab.file content
// */5 * * * * /usr/bin/php /var/www/html/motionpayAPI_PUB/cronjob.php
// 该程序用来检查订单是否已经被支付。您不需要这个程序，除非您无法获得异步通知。
// 要开启该功能，能只需要把该程序放到定时执行的程序列表里，请参考 crontab 命令的使用方法：
// crontab crontab.file
// crontab.file content
// */5 * * * * /usr/bin/php /var/www/html/motionpayAPI_PUB/cronjob.php

ini_set('date.timezone', 'Asia/Shanghai');
require_once "./lib/MotionPay.Config.php";
require_once "./lib/MotionPay.Api.php";
require_once "./lib/Log.php";

$logHandler = new CLogFileHandler(MotionPayConfig::getMotionPayLogFilename());
$log = Log::Init($logHandler, 15);

header("Content-Type:text/html;charset=utf-8");
$notifyURL = MotionPayConfig::getPaymentNotifyURL();
$filename = MotionPayConfig::getMotionPayOrderCSVFilename();
$delimiter = ",";

if(!file_exists($filename) || !is_readable($filename)) 
{
    echo "cannot find or open the file:" . $filename;
}
else
{
    $header = NULL;
    $data = array();
    if (($handle = fopen($filename, 'r')) !== FALSE)
    {
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
        {
            if(!$header)
                $header = $row;
            else
                $data[] = array_combine($header, $row);
        }
        fclose($handle);
    }
    
    $paidOrdercontents = file_get_contents(MotionPayConfig::getMotionPayPaidOrderCSVFilename());
    foreach($data as $order) {
        $mid = $order['mid'];
        $orderId = $order['orderId'];
        $paymentAmount = $order['paymentAmount'];
        $timestamp = $order['timestamp'];
        $comments = $order['comments'];
        
        echo "Found order for checking:" . "\n";
        echo "  mid:" . $mid . "\n";
        echo "  orderId:" . $orderId . "\n";
        echo "  paymentAmount:" . $paymentAmount . "\n";
        echo "  timestamp:" . $timestamp . "\n";
        echo "  comments:" . $comments . "\n";
        
        if (strpos($paidOrdercontents, $orderId) !== false) 
        {
            echo 'This order has been paid.' . "\n"; 
        }
        else 
        {
            echo 'This order has not been paid.' . "\n";
            $date = new DateTime();
            $timeStampNow = $date->getTimestamp();
            $differTime = $timeStampNow - $timestamp;
            echo 'This order placed ' . $differTime . " seconds ago.\n";
            if($differTime > 24 * 60 * 60) 
            {
                echo "Order placed 1 day ago. We needn't to check if it is paid. \n";
            }
            else
            {
                echo "Checking the order if it is paid now... \n";
                $url = $notifyURL . "?mid=" . $mid . "&orderId=" . $orderId . "&paymentAmount=" . $paymentAmount . "&";
                //Use file_get_contents to GET the URL in question.
                $contents = file_get_contents($url);
                
                //If $contents is not a boolean FALSE value.
                if($contents !== false){
                    //Print out the contents.
                    echo "This order is:" . $contents . "\n";
                    if($contents == 'paid') 
                    {
                        // You can put some logic here. But put the logic into paymentNotify.php is better.
                    }
                }
            }
        }
    }
}
    

