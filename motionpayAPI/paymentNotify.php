<?php
ini_set('date.timezone', 'Asia/Shanghai');
require_once "./lib/MotionPay.Config.php";
require_once "./lib/MotionPay.Data.php";
require_once "./lib/MotionPay.Api.php";
require_once './lib/Log.php';

$logHandler = new CLogFileHandler(MotionPayConfig::getMotionPayLogFilename());
$log = Log::Init($logHandler, 15);

$found = false;
$filename = MotionPayConfig::getMotionPayCallbackFilename();

$orderId = "";
$paymentAmount = "";
$mid = "";
$message = "";

// $orderId = "201802221729031964360860";
// $paymentAmount = "1";


if(isset($_GET['orderId'])) {
    $orderId = $_GET['orderId'];
}
if(isset($_GET['paymentAmount'])) {
    $paymentAmount = $_GET['paymentAmount'];
}
if(isset($_GET['mid'])) {
    $mid = $_GET['mid'];
}
if(strlen($orderId) == 0 || strlen($paymentAmount) == 0 || strlen($mid) == 0) {
    echo "mid, orderId and paymentAmount cannot be empty.\n";
}
else {
    if(file_exists($filename) == false) {
        echo "file doesn't exist:" . $filename . "\n";
    }
    else {
        foreach(file($filename) as $line) {
            if(strlen($line) > 0) {
                $pos = strpos($line, "out_trade_no");
                if($pos > 0) {
                    $info = "[info]";
                    $pos = strpos($line,$info);
                    $jsonInlog = substr($line,$pos + strlen($info));
                    $jsonStr = trim($jsonInlog);
                    
                    // echo "jsonStr:" . $jsonStr;
                    $arrayResult = json_decode($jsonStr, true);
                    $out_trade_no = $arrayResult['out_trade_no'];
                    $pay_result = $arrayResult['pay_result'];
                    $total_fee = $arrayResult['total_fee'];
                    
                    
                    // echo "out_trade_no" . $out_trade_no;  
                    // echo "orderId" . $orderId;  
                    if (strlen($out_trade_no) > 0 && $out_trade_no == $orderId) {
			        // echo "total_fee" . $total_fee;
                        if($pay_result == "SUCCESS" && $total_fee == $paymentAmount) {
                            $found = true;
                            break;
                        }
                    }
                }
            }
        }
    }
}

if($found == false) {
    /**
     * Use query function to check if the payment is done or not. It is possible the call back notify is failed so we need to use query function to confirm the order is paid and there
     * is no duplicated payment for the same order.
     * 我们使用查询订单的功能来确认订单是否已经支付。异步通知可能会失败，我们用下面的代码来查询订单是否已经被支付，以保证同一个订单不会被多次支付。
     */
    $input = new MotionPayOrderQuery();
    $input->setMerchantTypeByMerchantId($mid);
    $input->setOutTradeNo($orderId);
    $resultReturn = MotionPayApi::orderQuery($input);
    if ($resultReturn['code'] == '0') {
        $result = $resultReturn['content'];
        if($result["trade_status"] == 'SUCCESS') {
            echo "paid";
            $orderinfo = array($mid,$orderId,$paymentAmount);
            $log->writePaidOrderToCSVFile(MotionPayConfig::getMotionPayPaidOrderCSVFilename(), $orderinfo);
        }
        else {
            echo "not paid yet";
        }
    }
    else {
        $message = $resultReturn['message'];
        echo "no found:" . $message;
    }
}
else {
    /**
     * Use query function confirm the callback notify is not fake. If you don't want to check it, just set $check_fake_callback to false.
     * 我们使用查询订单的功能来确认订单是否已经支付。以防止伪造的异步通知，如果您不想做伪造的异步通知的检查，可以设置$check_fake_callback 为 false.
     */
    $check_fake_callback = true;
    if($check_fake_callback == true) {
        $input = new MotionPayOrderQuery();
        $input->setMerchantTypeByMerchantId($mid);
        $input->setOutTradeNo($orderId);
        $resultReturn = MotionPayApi::orderQuery($input);
        if ($resultReturn['code'] == '0') {
            $result = $resultReturn['content'];
            if($result["trade_status"] == 'SUCCESS') {
                echo "paid";
                $orderinfo = array($mid,$orderId,$paymentAmount);
                $log->writePaidOrderToCSVFile(MotionPayConfig::getMotionPayPaidOrderCSVFilename(), $orderinfo);
            }
            else {
                echo "not paid yet. Fake call back found!";
            }
        }
        else {
            $message = $resultReturn['message'];
            echo "no found:" . $message;
        }
    }
    else {
        echo "paid";
    }
}


